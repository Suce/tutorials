# Embeds

Now we're gonna learn all about embeds! 

Here's how an embed looks like:

![Embed Breakdown](./imgs/embed_breakdown.png)

Now, you see, embeds are very straight forward, to make them, we need to use the class `EmbedBuilder` like this:

```java
EmbedBuilder eb = new EmbedBuilder()
```

Here, we made a new `EmbedBuilder` to build an embed, now let's see how to change the color, and add fields, and add a thumbnail, and add a picture, and add an author, etc.

```java
eb.setColor(0xFF00FF);
// this will change the color of the embed
```

This is used to change the color of the embed. You need to enter a hex, and switch the `#` with `0x` so if you need to make it white, instead of `#FFFFFF` you'd type `0xFFFFFF`

```java
eb.setTitle("this is a title!");
// this will set the title of the embed to this is a title! 
```

This will set the title of the embed to the string inside the parentheses. If I wanted the title to say "Woo!" then I'd set the title of the embed to this: `eb.setTitle("Woo!")` its that easy!

```java
eb.setDescription("This is a description!");
// this will change the description of the embed to This is a description!
``` 

This will set the description to "This is a description!" and the description is under the title of the embed!

```java
eb.setAuthor("M.A", "iconurl");
// this sets the name of the author to M.A, and sets the icon of the author to a url!
```

This sets the `Author` attribute name to "M.A", and the icon to a url. Be sure to include `https://` in the url or it wont work.

```java
eb.setThumbnail("url");
// sets the thumbnail to a picture from a url
```

This sets the thumbnail to a picture from the url. Be sure to include `https://` in the link or it wont work.

```java
eb.addField("name of the field", "value of the field", False); // or you can put True instead
// you can choose True or False in the inline field (the 3rd argument, could use True or False, I did False)
```

This is an attribute that adds a field to your embed, it has 2 strings, and an inline boolean.

Here's an example on inline and non-inline fields:

![Embed Inline Fields](./imgs/embed_inline_fields.png)

As you can see, the inline fields are all in one line,

![Embed Non-Inline Field](./imgs/embed_non-inline_field.png)

But the not inline field takes another line.

The 2 strings `name` and `value` both show up in the example shown up, the name being the bold, the upper one, the value being the little transparent one.

![Embed Inline Empty Field](./imgs/embed_inline_empty_field.png)

Also, in JDA, you can add empty fields!  
To add empty fields in, you must do:

```java 
eb.addBlankField(True);
```

That'll make the field inline so its in the same line, 
> NOTICE: You can only put 3 inline fields at one line.

![Embed Non-Inline Empty Field](./imgs/embed_non-inline_empty_field.png)

This shows a blank not inline field!
To do that, the code would be something like this

```java
eb.addBlankField(False);
```

Thats all about blank fields! Now, moving on, to: **Embed Images!**

This is pretty easy, you just need to do:

```java
eb.setImage("url");
```

The URL must have the `https://` with it or it wouldn't show.

Now, onto footer!

![Embed Footer](./imgs/embed_footer.png)

This is a footer, its made using this:

```java
eb.setFooter("this is a footer", "iconurl");
```

This is a how you set the Footer, again, with the `iconURL` starting with `https://` 

![Embed Timestamp](./imgs/embed_timestamp.png)

Now, into timestamps!

Timestamps are made in JDA with:

```java
eb.setTimestamp(Instant.now());
```

This will make the timestamp take the exact time of when the embed was made and sets as the timestamp!

Now, this will set the attributes of the embed, but not put it to a message and share the message! to do that, we will code the following:

```java
MessageBuilder message = new MessageBuilder();

message.setEmbed(eb.build());
MessageChannel.sendMessage(message.build().queue();
```

> NOTICE: you can also chain these calls! For example:

```java
EmbedBuilder eb = new EmbedBuilder()
     .setTitle("This is a title :blush:")
     .setDescription("Im kewl")
     .setColor(0xC0FFEE)
    
// then later on we can do this

MessageBuilder message = new MessageBuilder();

message.setEmbed(eb.build());

MessageChannel.sendMessage(message.build().queue();
```

Now, for real, thats all about embeds!

---

Finally, to [**ReadyEvent and Logging**](./readyevent-and-logging.html)!
