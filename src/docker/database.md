# Database on Docker

## PostgreSQL

Ok, firstly I'll be showing a correctly working PostgreSQL setup
`docker-compose.yml`
```yml
version: "3"
services:
  bot:
    build: .
    environment:
      - TOKEN
      - DATABASE_URL=postgres://postgres:@postgresql:5432
    depends_on:
      - postgresql

  postgresql:
    image: postgres:alpine
```
`bot.py`
```python
import os
import time

import discord
from discord.ext import commands
import psycopg2

bot = commands.Bot(command_prefix='!')

conn = psycopg2.connect(os.getenv('DATABASE_URL'))

with conn:
    with conn.cursor() as cur:
        cur.execute('CREATE TABLE IF NOT EXISTS messages (name VARCHAR, message VARCHAR);')

@bot.command()
async def say(ctx, *, something):
    msg = f'{ctx.author} said: {something}'
    print(msg)
    await ctx.send(msg)

@bot.command()
async def save(ctx, name, *, something):
    with conn:
        with conn.cursor() as cur:
            cur.execute('INSERT INTO messages (name, message) values (%s,%s)', (name ,something))

@bot.command()
async def fetch(ctx, name):
    with conn:
        with conn.cursor() as cur:
            cur.execute('SELECT message FROM messages WHERE name=%s', (name,))
            await ctx.send(cur.fetchone())

bot.run(os.getenv('TOKEN'))
```
`Dockerfile`
```Dockerfile
FROM alpine
ADD . /src
WORKDIR /src
RUN apk add --no-cache python3 py3-psycopg2 py3-multidict; pip3 install -r requirements.txt
CMD ./wait_for.sh postgresql:5432 -- python3 bot.py
```
You will also need the `wait_for` script found [here](https://github.com/eficode/wait-for/blob/master/wait-for). Notice how little I had to add to `docker-comopose.yml` to get a PostgreSQL database set up.

We add a new service that I'm calling `postgresql` and then after that we are specifying an `image:` instead of build because we want to just use the pre-setup postgresql database image from Docker Hub. We add a new environment variable to the bot service so it knows where to find the database, I'm using URL notation for this, `protocol://username:password@domain:port` of course it's the postgres protocol, and the `postgres:alpine` image creates the postgres user, the password is currently blank (don't worry only other services in this compose cluster will be able to connect, then the domain is the name of the service in docker-compose which is currently postgresql, 5432 is just the default port. The reason I have now added the wait_for.sh to the Dockerfile to ensure that the bot waits until the database is ready before starting up its self overwise you will get errors and bot may crash if it starts before the database, but that `depends_on:` bit in the `docker-compose.yml` doesn't quite serve this purpose correctly :( But fingers crossed, if you run
```
docker-compose up --build
```
It should just work.

## Redis
`bot.py`
```python
import os

import discord
from discord.ext import commands
import redis

bot = commands.Bot(command_prefix='!')

rcon = redis.StrictRedis(host='redis')

@bot.command()
async def say(ctx, *, something):
    msg = f'{ctx.author} said: {something}'
    print(msg)
    await ctx.send(msg)

@bot.command()
async def save(ctx, name, *, something):
    rcon.set(name, something)

@bot.command()
async def fetch(ctx, name):
    await ctx.send(rcon.get(name))

bot.run(os.getenv('TOKEN'))
```
`Dockerfile`
```Dockerfile
FROM alpine
ADD . /src
WORKDIR /src
RUN apk add --no-cache python3 py3-multidict; pip3 install -r requirements.txt
CMD ./wait_for.sh redis:6379 -- python3 bot.py
```
`docker-compose.yml`
```yml
version: "3"
services:
  bot:
    build: .
    environment:
      - TOKEN

  redis:
    image: redis:alpine
```
`redis` will also need adding to your requirements.txt if you are following this code you also need the wait_for.sh from link above. In the compose file we are adding a redis service with the image `redis:alpine` then in the `bot.py` we connect to it using its hostname (name of the service in compose file), wait_for is used to make sure redis is started before the bot starts
```
docker-compose up --build
```
And it should all now work. Simples!

---

# The end

For now this concludes my Docker tutorial because I'm unsure what else I want to add. I welcome requests for what you want to know about Mr/Mrs reader.